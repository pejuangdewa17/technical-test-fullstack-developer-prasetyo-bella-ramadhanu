@extends('layout.app')

@section('css')
<link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection
@section('breadcrumb')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Karyawan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a></li>
        <li class="breadcrumb-item active" aria-current="page">Karyawan</li>
    </ol>
</div>
@endsection
@section('content')
<div class="card sm mb-4">
    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary"></h6>
    </div>
    <div class="card-body">
        <a href="{{ route('employee.create') }}" id="add" class="btn btn-primary">+ Tambah Data</a>
        <div class="table-responsive p-3">
            <table class="table align-items-center table-flush" id="dataTableEmployee">
                <thead class="thead-light">
                    <tr>
                        <th>No</th>
                        <th>NIP</th>
                        <th>Posisi</th>
                        <th>Nama</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>


{{-- Modal --}}
{{-- Add --}}
<div class="modal fade none-border" id="addModal">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5><strong>Tambah Data</strong></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <form id="form-add">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <label class="control-label">
                                Nama Karyawan
                            </label>
                            <input class="form-control form-white" type="text" name="nama_barang" id="nama_barang_acc"
                                required />
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">
                                Harga Beli
                            </label>
                            <input class="form-control form-white" type="number" name="harga_beli" id="harga_beli_acc"
                                required />
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">
                                Harga Jual
                            </label>
                            <input class="form-control form-white" type="number" name="harga_jual" id="harga_jual_acc"
                                required />
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">
                                Merk
                            </label>
                            <input class="form-control form-white" type="text" name="merk" id="merk_acc" />
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">
                                Stok
                            </label>
                            <input class="form-control form-white" type="number" name="stok_barang" id="stok_barang_acc"
                                required />
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">
                                Supplier
                            </label>
                            <select class="js-example-basic-single" name="supplier_id" style="width: 100%;"
                                id="supplier_idAdd">

                            </select>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">
                                Kategori
                            </label>
                            <select class="js-example-basic-single" name="kategori_id" style="width: 100%;"
                                id="kategori_idAdd">

                            </select>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">
                                Satuan
                            </label>
                            <select class="js-example-basic-single" name="satuan_id" style="width: 100%;"
                                id="satuan_idAdd">

                            </select>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">
                                Diskon
                            </label>
                            <input class="form-control form-white" type="number" name="diskon" id="diskon_acc" />
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

                <button id="btn-submit" type="button" class="btn btn-success waves-effect waves-light save-category"
                    data-dismiss="modal" onclick="addPost()">Simpan</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
{{-- <script src="https://code.jquery.com/jquery-3.6.0.js"
    integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script> --}}
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script>
    $('#dataTableEmployee').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('employee.index') }}",
            columns: [{
                    data: 'DT_RowIndex',
                    orderable: false,
                    searchable: false,
                    className: "text-center"
                },
                {
                    data: 'nip'
                },
                {
                    data: 'relasi_position.name'
                },
                {
                    data: 'name_employee'
                },
                {
                    data: 'action'
                },
            ]
        })
</script>
@endpush
