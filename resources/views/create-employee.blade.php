@extends('layout.app')

@section('css')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection
@section('breadcrumb')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Karyawan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a></li>
        <li class="breadcrumb-item active" aria-current="page">Karyawan</li>
    </ol>
</div>
@endsection
@section('content')
@if (count($errors) > 0)
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="card sm mb-4">
    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary"></h6>

    </div>
    <div class="card-body">
        <form action="{{ route('employee.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <label class="control-label">
                        NIP
                    </label>
                    <input class="form-control form-white" type="text" name="nip" id="nip" required />
                </div>
                <div class="col-md-6">
                    <label class="control-label">
                        Nama Karyawan
                    </label>
                    <input class="form-control form-white" type="text" name="name_employee" id="name_employee"
                        required />
                </div>
                <div class="col-md-6">
                    <label class="control-label">
                        Jabatan
                    </label>
                    <select class="form-control" name="position_id" style="width: 100%;" id="position_id">
                        <option selected disabled>---</option>
                        @foreach ($data as $d)
                        <option value="{{ $d->id }}">{{ $d->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-6">
                    <label class="control-label">
                        Departemen
                    </label>
                    <input class="form-control form-white" type="text" name="departemen" id="departemen" required />
                </div>
                <div class="col-md-6">
                    <label class="control-label">
                        Tanggal lahir
                    </label>
                    <input class="form-control form-white" type="date" name="date_of_birth" id="date_of_birth"
                        required />
                </div>
                <div class="col-md-6">
                    <label class="control-label">
                        Tahun lahir
                    </label>
                    <select class="form-control" name="year_of_birth" id="year_of_birth" required>

                    </select>
                </div>
                <div class="col-md-6">
                    <label class="control-label">
                        Alamat
                    </label>
                    <input class="form-control form-white" type="text" name="alamat" id="alamat" required />
                </div>
                <div class="col-md-6">
                    <label class="control-label">
                        No Telp
                    </label>
                    <input class="form-control form-white" name="no_telp" id="no_telp" type="number"
                        ondrop="return false;" onpaste="return false;"
                        onkeypress="return event.charCode>=48 && event.charCode<=57" required />
                </div>
                <div class="col-md-6">
                    <label class="control-label">Agama</label>
                    <div class="row">
                        <div class="col-1">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="agama" id="agama1">
                                <label class="form-check-label" for="agama1">
                                    Islam
                                </label>
                            </div>
                        </div>
                        <div class="col-1">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="agama" id="agama2">
                                <label class="form-check-label" for="agama2">
                                    Katolik
                                </label>
                            </div>
                        </div>
                        <div class="col-1">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="agama" id="agama3">
                                <label class="form-check-label" for="agama2">
                                    Protestan
                                </label>
                            </div>
                        </div>
                        <div class="col-1">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="agama" id="agama3">
                                <label class="form-check-label" for="agama3">
                                    Hindu
                                </label>
                            </div>
                        </div>
                        <div class="col-1">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="agama" id="agama4">
                                <label class="form-check-label" for="agama4">
                                    Budha
                                </label>
                            </div>
                        </div>
                        <div class="col-1">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="agama" id="agama5">
                                <label class="form-check-label" for="agama5">
                                    Konghuchu
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <label class="control-label">
                        Status
                    </label>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" id="status1" value="1" name="status">
                        <label class="form-check-label" for="status1">
                            Aktif
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" id="status2" value="0" name="status">
                        <label class="form-check-label" for="status2">
                            Tidak Aktif
                        </label>
                    </div>
                </div>
                <div class="col-md-12">
                    <label class="control-label">
                        File KTP
                    </label>
                    <input type="file" name="foto_ktp" id="foto_ktp" class="form-control">
                </div>
                <div class="col-md-12 mt-5">
                    <button type="submit" class="btn btn-primary w-100">SIMPAN DATA</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@push('js')
<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
    crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
    $(document).ready(function() {

            $.get('{{ url('api/get-position') }}', function(data) {
                var trPositon = '<option></option>';
                $.each(data, function(i, post) {
                    trPositon += '<option value="' + post.id + '">' + post
                        .name + '</option>';
                })
                $('#position_id').empty().append(trPositon);
            })

            var start = 1900;
            var end = new Date().getFullYear();
            var options = "<option disabled selected>---</option>";
            for (var year = start; year <= end; year++) {
                options += "<option value=" + year + ">" + year + "</option>";
            }
            document.getElementById("year_of_birth").innerHTML = options;
        })
</script>
<script>
    $(function() {
            $('#only-number').on('keydown', '#number', function(e) {
                -1 !== $
                    .inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) || /65|67|86|88/
                    .test(e.keyCode) && (!0 === e.ctrlKey || !0 === e.metaKey) ||
                    35 <= e.keyCode && 40 >= e.keyCode || (e.shiftKey || 48 > e.keyCode || 57 < e
                    .keyCode) && (96 > e.keyCode || 105 < script e.keyCode) && e.preventDefault()
            });
        })
</script>
@endpush
