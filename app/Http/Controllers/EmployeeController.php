<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Position;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            $queryEmployees = Employee::with([
                'relasi_position'
            ])->latest()->get();
            return datatables($queryEmployees)
                ->addColumn(
                    'action',
                    function ($data) {
                        $btnAction = '<a href="#" class="btn btn-warning mb-3" id="btn-edit" data-id="' . $data->id . '" title="Edit Data"> Edit</a>';
                        $btnAction .= '<br>';
                        $btnAction .= '<a href="#" class="btn btn-info mb-3" id="btn-show" data-id="' . $data->id . '" title="Show Data"> Show</a>';
                        $btnAction .= '<br>';
                        $btnAction .= '<a href="#" id="btn-delete" class="btn btn-danger" data-id="' . $data->id . '" title="Hapus Data"> Hapus</a>';
                        return $btnAction;
                    }
                )
                ->rawColumns([
                    'action'
                ])
                ->addIndexColumn()
                ->make(true);
        }
        return view('employee');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Position::all();
        return view('create-employee', [
            'data' => $data
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => ':attribute wajib diisi cuy!!!',
            'unique' => ':attribute sudah ada',
        ];

        $this->validate($request, [
            'nip' => 'required|min:5|unique:employees,nip',
        ], $messages);
        Employee::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        //
    }
}
