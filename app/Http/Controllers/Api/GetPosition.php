<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Position;
use Illuminate\Http\Request;

class GetPosition extends Controller
{
    public function index()
    {
        return Position::all();
    }
}
