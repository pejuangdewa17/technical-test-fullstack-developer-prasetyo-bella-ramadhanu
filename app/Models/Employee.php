<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;
    protected $fillable = [
        'name_employee',
        'nip',
        'position_id',
        'departemen',
        'date_of_birth',
        'year_of_birth',
        'alamat',
        'no_telp',
        'agama',
        'status',
        'foto_ktp'
    ];

    public function relasi_position()
    {
        return $this->belongsTo(Position::class, 'position_id', 'id');
    }
}
